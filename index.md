---
layout: home
summary: Plume Rédaction
subtitle: Plateforme de publication au service de la fonction publique
---

## Par et pour les services publics
Plume est une plateforme de publication de document **open-source**, permettant à plusieurs rédacteurs de collaborer sur un document et d'y appliquer en toute simplicité la charte graphique en place dans leur administration.

Il est basé sur le framework open-source de la [Collaborative Knowledge Foundation](https://coko.foundation/) et développé à la [Cour des Comptes](https://www.ccomptes.fr/fr) dans le cadre du programme des [Entrepreuneurs d'Intérêt Général](https://entrepreneur-interet-general.etalab.gouv.fr/index.html).