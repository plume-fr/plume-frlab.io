---
layout: page
title: Tester Plume
---

## Utilisateur

Une plateforme de démonstration est à votre disposition pour tester Plume : [ici](https://plume-redaction.eig-forever.org) 

Utilisez pour cela les identifiants suivants : 

 - username : v1
 - password : azertyuiop

## Développeur 

Vous pouvez installer Plume sur votre machine en suivant les indications du README présent sur le dépot gitlab du projet. 
Nous sommes à votre disposition pour répondre à toute vos questions par [mail](mailto:erica.marco@ccomptes.fr) ou sur [TCHAP](https://www.tchap.gouv.fr/#/room/#CCEIGPlume2019omagKOv:agent.tchap.gouv.fr) 
