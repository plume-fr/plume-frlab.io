---
layout: page
title: Avancement du projet Sprint par Sprint
---



## En cours : Version 0.16 du 4 au 18 décembre


## Version 0.15 du 20 novembre au 3 décembre

### Évenements notables :

- **Dépot d'un RAR rédigé sur Plume par Guy Dugueperoux !**
- Participation à la rencontre de la communauté open-source à laquelle nous participons
	- Nikos Peteinatos et Guy Dugueperoux se sont rendu à Chicago pour rencontrer les autres utilisateurs de Editoria (dont Plume est une variation adaptée pour la Cour des Comptes)

### Évolutions visibles

  - Ajout d'information de métadonnées (Chambre, section, identifiant Argos)*
  - Création de modéle de document configurables via l'interface utilisateur*
  - Mise à jour du systéme d'export et de prévisualisation**
  - Possibilité de gérer (créer, modifier, déplacer) des chapitres et sous chapitres depuis le sommaire**
  - Séparation du sommaire en 3 sections : Préface, Rapport, Annexes*
    
\* amélioration développée par la communauté et intégrée à notre projet
** en cours de stabilisation

### Évolutions techniques :

-  intégration transparente de notre éditeur dans l'éditeur open-source (internationalisation, paramétrage des composants, configuration des barres d'outils) 


## Version 0.14 du 30 octobre au 19 novembre

### Évenements notables :

- restitution des projets [Entrepreneur d'Intérêt Général](https://entrepreneur-interet-general.etalab.gouv.fr/defis/2019/plume.html)
	- le programme s'est terminé et nous continuons en tant que contractuel de la Cour
- participation à la visite de la Cour Europeenne

### Évolutions visibles

   Ce sprint a été particulier en ce que nous sommes sur le point d'intégrer 3 mois de développement extérieur, fruit du travail de la Collaborative Knowledge Foundation avec laquelle nous collaborons. Ce travail d'intégration se distingue des habituelles fonctionnalités ajoutées successivement et nous devons donc stabiliser l'ensemble avant de vous présenter les nouveautés incluses. 
Ce sera donc pour le prochain sprint (retrospective le 3 décembre) mais vous allez voir, il y en aura beaucoup ;)



## Version 0.13 du 16 au 29 octobre

### Évenements notables : 
- présentation aux représentants CAM de la Cour des Comptes
- rencontre avec le pole éditorial du SRPP


### Évolutions visibles

- correction de l'export pdf (qui entrainera une amélioration de performance)
- correction des retours à la ligne intempestifs dans le rapport généré
- mise à jour de l'interface de login et de création de comptes
- possibilité de définir l'emplacement de la table des matières et de la table des recommandations à n'importe quel point du texte
- gestion des espaces insécables 
- uniformisation des interlignes


### Évolutions techniques :

-  automatisation du déploiement 
-  épuration du document HTML généré

## Version 0.12 du 2 au 15 octobre

### Événements notables :

- **dépôt du premier ROP rédigé sur Plume !**
- openlabs en CRC Haut de France
- présentation du produit aux référents CRTC


### Évolutions :

- ajout de la feuille de styles ROP
- possibilité de visualiser le document et ses révisions sous format PDF 
- amélioration du mode de lecture "révisions invisible" depuis l'éditeur
- ajout de l'avant propos dans les RIOP et ROP
- amélioration de la page de garde

## Version 0.11 du 18 septembre au 1 octobre

### Événements notables :
- congés de la développeuse, mise en pause des développements

## Version 0.10 du 4 au 17 septembre 

**jours de développement**: 3

### Événements notables : 
- rencontre COCAM
- lancement de la rédaction d'un nouveau document par notre alpha-testeur
- depart en congés de la développeuse

### Evolutions :
- ajout de liens permettant la navigation depuis la page "Recommandations"
- paramétrabilité de la langue par défaut
- déploiement de deux serveurs à la Cour : bac à sable et pré-production
	- **bac à sable** : ce serveur sera mis à jour toute les deux semaines afin de rendre accessibles les nouvelles fonctionnalités dés leur sortie pour qu'elles puissent être testées et validées par les utilisateurs. 
	- **pré-production** : ce serveur sera mis à jour avec une semaine de décalage par rapport au bac à sable afin d'assurer sa stabilité. C'est ici que seront rédigés les rapports dans le cadre des alphas-tests.

## Version 0.9 du 21 aout au 3 septembre

**jours de développement** : 6

### Événements notables : 
- participation à la rencontre trimestrielle de la communauté Pubsweet 
- rencontre avec l'AFNOR

### Evolutions :
- maj : scripts de déploiement Centos et Debian
- branchement de l'éditeur Wax2 (version béta)
- maj : mise en page de l'export PDF 
- migration de l'environnement de developpement vers Windows 10 

## Version 0.8 du 7 au 20 aout

### Evolutions :
 - possibilité de choisir entre plusieurs structures de textes
 - gestion des notes de bas de page
 - ajout de la table des matières
 - installation automatisée pour l'environnement Centos 7

## Version 0.7 du 24 juillet au 6 aout

### Événements notables : 

- dépot du premier controle rédigé avec l'outil Plume
- présentation de l'avancement du projet au secrétaire général
- arrivée d'un nouveau contributeur

### Evolutions :
- amélioration graphique de l'éditeur
- ajout de nouvelles valeurs sémantiques (titre non-numérotés, observations...)
- ajout d'une gestion basique des tableaux
- ajout de la table de recommandation
- possibilité de choisir entre plusieurs styles graphique
- gestion basique des droits (rédacteur/relecteur) \*


\* amélioration développée par la communauté et intégrée à notre projet
